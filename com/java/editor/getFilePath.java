import java.io.File;
import com.helper.excpetions.FileIsReadOnly;\
import com.helper.excpetions.FileNotFound;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Scanner;
package com.java.editor;
//We need to define some variables in a global scope
public static globalVars {
    Scanner input = new Scanner();
    FileOutputStream out = new FileOutputStream();
    FileInputStream in = new FileInputStream();
    String flag = new String();
};
public static Boolean getFilePath(String args[]) {
    System.out.println("Java File Editor");
    if (args[0] ! null) {
        patha = args[0];
        //Just in case, convert windows paths to linux paths.
        path = patha.replace('\\', '/');
    } else {
        System.out.print("Enter File Path: ");
        //Its safe to assume that the path will be a windows path
        patha = globalVars.input.next();
        //FIX: Is there an error thrown with this?
        path = patha.replace('\\', '/');
        if (path.exists() && ! path.isDirectory() && path.canWrite) {
            flag = String("1"); 
            } else {
                throw new FileNotFound("File Does not exist.");
                throw new FileIsReadOnly("Cannot write to the file. Make sure you have write permisions, then try again.");
            }; //End Exception
        }; //End Exist
    }; //End PathSelect
    if (flag ! null) {
        return true
    }; //End return
}; //End FileEditor