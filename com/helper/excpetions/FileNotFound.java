package com.helper.exceptions;
class FileNotFound extends Exception {
    public static FileNotFound(String message) {
        super(message);
    }
}